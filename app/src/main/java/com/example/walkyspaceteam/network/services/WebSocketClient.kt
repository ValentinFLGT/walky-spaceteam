package com.example.walkyspaceteam.network.services

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import android.util.Log
import com.example.walkyspaceteam.model.actions.Action
import com.example.walkyspaceteam.model.elements.UIElement
import com.example.walkyspaceteam.model.events.Event
import com.example.walkyspaceteam.model.users.User
import org.json.JSONObject

class WebSocketClient : WebSocketListener() {

    // Livedata used by viewModels to update UI
    var userList = MutableLiveData<List<User>>()
    var gameStarted = MutableLiveData<List<UIElement>>()
    var nextAction = MutableLiveData<Action>()
    var gameOver = MutableLiveData<Event.GameOver>()
    var nextLevel = MutableLiveData<Event.NextLevel>()

    private val _message = MutableLiveData<String>()
    val message: LiveData<String>
        get() = _message

    private val moshi = MoshiClient.moshiEvent

    // Overrided methods from the WebSocketListener interface
    override fun onOpen(webSocket: WebSocket, response: Response) {
        super.onOpen(webSocket, response)
        Log.i("WebSocketOpen", response.toString())
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        super.onMessage(webSocket, text)

        val event = JSONObject(text)

        when (event["type"]) {

            "NEXT_ACTION" -> {
                val myEvent =
                    moshi.adapter<Event.NextAction>(Event.NextAction::class.java).fromJson(text)
                if (myEvent != null) {
                    updateNextAction(myEvent.action)
                }
            }

            "GAME_STARTED" -> {
                val myEvent =
                    moshi.adapter<Event.GameStarted>(Event.GameStarted::class.java).fromJson(text)
                if (myEvent != null) {
                    updateGameStarted(myEvent.uiElementList)
                }
            }

            "GAME_OVER" -> {
                val myEvent =
                    moshi.adapter<Event.GameOver>(Event.GameOver::class.java).fromJson(text)
                if (myEvent != null) {
                    updateGameOver(myEvent)
                }
            }

            "NEXT_LEVEL" -> {
                val myEvent =
                    moshi.adapter<Event.NextLevel>(Event.NextLevel::class.java).fromJson(text)
                if (myEvent != null) {
                    updateNextLevel(myEvent)
                }
            }

            "WAITING_FOR_PLAYER" -> {
                val myEvent =
                    moshi.adapter<Event.WaitingForPlayer>(Event.WaitingForPlayer::class.java)
                        .fromJson(text)
                if (myEvent != null) {
                    updateUserList(myEvent.userList)
                }
            }

            "ERROR" -> {
                var myEvent = moshi.adapter<Event.Error>(Event.Error::class.java).fromJson(text)
            }

            "READY" -> {
                var myEvent = moshi.adapter<Event.Ready>(Event.Ready::class.java).fromJson(text)
            }

            "PLAYER_ACTION" -> {
                var myEvent =
                    moshi.adapter<Event.PlayerAction>(Event.PlayerAction::class.java).fromJson(text)
            }
        }
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        super.onClosing(webSocket, code, reason)
        webSocket.close(1000, null);
        output("Closing : $code / $reason");
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        super.onFailure(webSocket, t, response)
        output("Error : " + t.message);
    }

    private fun output(txt: String) {
        _message.postValue(txt)
    }

    private fun updateUserList(ul: List<User>) {
        userList.postValue(ul)
    }

    private fun updateGameStarted(lui: List<UIElement>) {
        gameStarted.postValue(lui)
    }

    private fun updateNextAction(na: Action) {
        nextAction.postValue(na)
    }

    private fun updateGameOver(go: Event.GameOver) {
        gameOver.postValue(go)
    }

    private fun updateNextLevel(nl: Event.NextLevel) {
        nextLevel.postValue(nl)
    }

    fun replay(){
        gameStarted = MutableLiveData<List<UIElement>>()
        nextAction = MutableLiveData<Action>()
        gameOver = MutableLiveData<Event.GameOver>()
        nextLevel = MutableLiveData<Event.NextLevel>()
        userList = MutableLiveData<List<User>>()
    }
}

// Remote function linked to the WebSocket in order not to instantiate a new WebSocket
fun sendMessage(webSocket: WebSocket, event: Event) {
    sendObjectToJson(event)?.let { webSocket.send(it) }
}

fun sendObjectToJson(event: Event): String? {
    val moshi = MoshiClient.moshiEvent
    val jsonAdapter = moshi.adapter<Event>(Event::class.java)
    return jsonAdapter.toJson(event)
}