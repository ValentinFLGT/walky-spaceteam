package com.example.walkyspaceteam.network

import com.example.walkyspaceteam.model.users.User
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

private const val BASE_URL = "https://spacedim.async-agency.com"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface SpaceDIMApiService {
    @GET("api/users")
    suspend fun getUsersList(@Query("sort") sort: String? = null): List<User>

    @GET ("/api/user/find/{name}")
    suspend fun getUserByName(@Path("name") name: String): User

    @GET ("/api/user/{id}")
    suspend fun loginUserByID(@Path("id") id: Int): User

    @Headers("Content-Type: application/json")
    @POST("/api/user/register")
    suspend fun registerUserByName(@Body requestBody: RequestBody): User
}

object UserApi {
    val retrofitService : SpaceDIMApiService by lazy {
        retrofit.create(SpaceDIMApiService::class.java)
    }
}