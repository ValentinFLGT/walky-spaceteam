package com.example.walkyspaceteam.viewmodel

import android.text.Editable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.walkyspaceteam.network.UserApi
import com.example.walkyspaceteam.model.actions.Action
import com.example.walkyspaceteam.model.elements.UIElement
import com.example.walkyspaceteam.model.events.Event
import com.example.walkyspaceteam.model.users.User
import com.example.walkyspaceteam.network.services.WebSocketClient
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.WebSocket
import org.json.JSONObject
import java.lang.Exception

class GameViewModel : ViewModel() {

    private val webSocketUrl = "ws://spacedim.async-agency.com:8081/"
    private val listener = WebSocketClient()

    // Observer functions declared in WebSocketClient
    fun getUserList(): LiveData<List<User>> = listener.userList
    fun getGameStarted(): LiveData<List<UIElement>> = listener.gameStarted
    fun getNextAction(): LiveData<Action> = listener.nextAction
    fun getGameOver(): LiveData<Event.GameOver> = listener.gameOver
    fun getNextLevel(): LiveData<Event.NextLevel> = listener.nextLevel

    // Livedata for the application
    private val _username = MutableLiveData<String>("")
    val username: LiveData<String>
        get() = _username

    private val _currentLevel = MutableLiveData<Int>()
    val currentLevel: LiveData<Int>
        get() = _currentLevel

    private val _user = MutableLiveData<User>()
    val user: LiveData<User>
        get() = _user

    private val _roomName = MutableLiveData<String>("")
    val roomName: LiveData<String>
        get() = _roomName

    private val _webSocket = MutableLiveData<WebSocket>()
    val webSocket: LiveData<WebSocket>
        get() = _webSocket

    private val _room = MutableLiveData<String>("")
    val room: LiveData<String>
        get() = _room

    private val _eventShake = MutableLiveData<Boolean>()
    val eventShake: LiveData<Boolean>
        get() = _eventShake

    private val _score = MutableLiveData<Int>()
    val score: LiveData<Int>
        get() = _score
    
    private val _highscore = MutableLiveData<List<User>>()
    val highscore: LiveData<List<User>>
        get() = _highscore

    private val _socketDot = MutableLiveData<Boolean>(false)
    val socketDot: LiveData<Boolean>
        get() = _socketDot
    
    // Livedata initiation
    init {
        _eventShake.value = false
        _currentLevel.value = 1
        _score.value = 0
    }
    
    // Livedata setters
    fun setRoom(s: Editable) {
        _room.value = s.toString()
    }

    fun setPseudo(userName: Editable) {
        _username.value = userName.toString()
    }

    fun setUser(user: User) {
        _user.value = user
    }

    fun resetUser(){
        _user.value = null
    }

    fun setRoomName(roomName: String) {
        _room.value = roomName
    }

    fun setWebSocket(ws: WebSocket) {
        _webSocket.value = ws
    }

    fun setCurrentLevel(currentLevel: Int) {
        _currentLevel.value = currentLevel
    }

    fun setScore(score: Int) {
        _score.value = score
    }

    fun setHighScore(listuser: List<User>) {
        _highscore.value = listuser
    }

    fun setSocketDot(state: Boolean) {
        _socketDot.value = state
    }

    fun getUserByName() {
        viewModelScope.launch {
            try {
                val userResult = UserApi.retrofitService.getUserByName(username.value.toString())
                loginUser(userResult.id)
            } catch (e: Exception) {
                registerUser(username.value.toString())
            }
        }
    }

    fun getUsersList() {
        viewModelScope.launch {
            val highscore = UserApi.retrofitService.getUsersList("top")
            setHighScore(highscore)
        }
    }

    private fun loginUser(id: Int) {
        viewModelScope.launch {
            val loggedUser = UserApi.retrofitService.loginUserByID(id)
            setUser(loggedUser)
        }
    }

    private fun registerUser(name: String) {
        viewModelScope.launch {
            val jsonObject = JSONObject()
            jsonObject.put("name", name)

            // Convert JSONObject to String
            val jsonObjectString = jsonObject.toString()

            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
            val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())
            val registeredUser = UserApi.retrofitService.registerUserByName(requestBody)
            setUser(registeredUser)
        }
    }

    fun connectToRoom(): LiveData<String> {
        viewModelScope.launch {
            val request: Request =
                Request.Builder()
                    .url(webSocketUrl + "ws/join/${room.value}/${user.value?.id}")
                    .build()

            setWebSocket(OkHttpClient().newWebSocket(request, listener))
            setSocketDot(true)
        }
        return listener.message
    }

    fun cheatConnectToRoom(id: Int): LiveData<String> {
        viewModelScope.launch {
            val request: Request =
                Request.Builder()
                    .url(webSocketUrl + "ws/join/${room.value}/${id}")
                    .build()

            setWebSocket(OkHttpClient().newWebSocket(request, listener))
        }
        return listener.message
    }

    fun onEventShake() {
        _eventShake.value = true
    }
    fun onEventShakeComplete() {
        _eventShake.value = false
    }

    fun onReplay(){
        setScore(0)
        setCurrentLevel(1)
        setSocketDot(false)
        listener.replay()
    }
}