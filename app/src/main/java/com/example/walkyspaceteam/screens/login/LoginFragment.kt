package com.example.walkyspaceteam.screens.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.example.walkyspaceteam.R
import com.example.walkyspaceteam.databinding.FragmentLoginBinding
import com.example.walkyspaceteam.model.users.User
import com.example.walkyspaceteam.viewmodel.GameViewModel
import com.google.android.material.snackbar.Snackbar


class LoginFragment : Fragment() {

    private var binding: FragmentLoginBinding? = null

    private val gameViewModel: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val fragmentBinding = FragmentLoginBinding.inflate(inflater, container, false)
        binding = fragmentBinding

        setHasOptionsMenu(true)

        return fragmentBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            lifecycleOwner = viewLifecycleOwner

            viewModel = gameViewModel

            loginFragment = this@LoginFragment

        }

        binding!!.playButton.setOnClickListener {
            if(gameViewModel.username.value.toString() !== ""){
                gameViewModel.getUserByName()
            } else {
                val snack = Snackbar.make(it,"Veuillez rentrer un pseudo",Snackbar.LENGTH_SHORT)
                snack.show()
            }
        }

        gameViewModel.user.observe(viewLifecycleOwner, Observer<User> { user ->
            if (user != null){
                view.findNavController().navigate(R.id.action_loginFragment_to_waitingRoomFragment)
            }
        })

    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}