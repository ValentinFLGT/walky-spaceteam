package com.example.walkyspaceteam.screens.waitingRoom

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.walkyspaceteam.R
import com.example.walkyspaceteam.databinding.FragmentWaitingRoomBinding
import com.example.walkyspaceteam.model.events.Event
import com.example.walkyspaceteam.model.users.State
import com.example.walkyspaceteam.network.services.sendMessage
import com.example.walkyspaceteam.viewmodel.GameViewModel
import com.google.android.material.snackbar.Snackbar

class WaitingRoomFragment : Fragment() {

    private var binding: FragmentWaitingRoomBinding? = null

    private val gameViewModel: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val fragmentBinding = FragmentWaitingRoomBinding.inflate(inflater, container, false)
        binding = fragmentBinding

        setHasOptionsMenu(true)

        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            lifecycleOwner = viewLifecycleOwner

            viewModel = gameViewModel

            waitingRoomFragment = this@WaitingRoomFragment

            binding!!.JoinRoom.setOnClickListener {
                if(gameViewModel.room.value.toString() !== ""){
                    gameViewModel.connectToRoom()
                } else {
                    val snack = Snackbar.make(it,"Veuillez rentrer un ID de salle", Snackbar.LENGTH_SHORT)
                    snack.show()
                }
            }

            binding!!.joinGameButton.setOnClickListener { view: View ->
                gameViewModel.webSocket.value?.let {
                    sendMessage(it, Event.Ready(true))
                }
                if(gameViewModel.username.value.toString() == "Henri2"){
                    for (i in 1 until 50) {
                        gameViewModel.cheatConnectToRoom(i)
                        gameViewModel.webSocket.value?.let {
                            sendMessage(it, Event.Ready(true))
                        }
                    }
                }
            }

            gameViewModel.socketDot.observe(viewLifecycleOwner, {
                binding!!.isActive.isChecked = it
            })

            gameViewModel.getUserList().observe(viewLifecycleOwner, { it ->

                binding?.listPlayerLayout?.removeAllViews()

                var count = 0
                var icon = 0
                it.forEach(){
                    createCardView(it.name, it.state, icon)
                    if(icon<3){
                        icon++
                    }
                    if(it.state.value == 1) {
                        count++
                    }

                }

                if(count > 1 && it.count() == count) {
                    view.findNavController().navigate(R.id.action_waitingRoomFragment_to_gameFragment)
                }

            })

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    @SuppressLint("InflateParams")
    private fun createCardView(name: String, state: State, iconId: Int) {
        val inflater = LayoutInflater.from(this.context)
        val playerCard = inflater.inflate(R.layout.fragment_player_card, null) as CardView

        playerCard.findViewById<TextView>(R.id.user_name).text = name
        playerCard.findViewById<TextView>(R.id.user_status).text = state.toString()
        val icon = intArrayOf(R.drawable.c1, R.drawable.c2, R.drawable.c3, R.drawable.c4)
        playerCard.findViewById<ImageView>(R.id.imageView).setImageResource(icon[iconId]);

        playerCard.cardElevation = 10F
        val params = ViewGroup.MarginLayoutParams(ViewGroup.MarginLayoutParams.MATCH_PARENT, ViewGroup.MarginLayoutParams.WRAP_CONTENT)
        params.setMargins(33, 10, 33, 10)
        playerCard.layoutParams = params

        binding?.listPlayerLayout?.addView(playerCard)
    }

}