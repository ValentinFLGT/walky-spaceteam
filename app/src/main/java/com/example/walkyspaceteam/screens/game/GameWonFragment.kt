package com.example.walkyspaceteam.screens.game

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.walkyspaceteam.R
import com.example.walkyspaceteam.databinding.FragmentGameWonBinding
import com.example.walkyspaceteam.model.users.User
import com.example.walkyspaceteam.viewmodel.GameViewModel


class GameWonFragment : Fragment() {

    private var binding: FragmentGameWonBinding? = null

    private val model: GameViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val fragmentBinding = FragmentGameWonBinding.inflate(inflater, container, false)

        binding = fragmentBinding

        setHasOptionsMenu(true)

        return binding!!.root

    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            lifecycleOwner = viewLifecycleOwner
            binding!!.gameWonScoreInformation.text =
                "Votre score pour cette partie était " + model.score.value.toString()
            model.getUsersList()
            binding!!.gameWonRetryButton.setOnClickListener { view : View ->
                model.onReplay()
                view.findNavController().navigate(R.id.waitingRoomFragment)
                model.connectToRoom()
            }
            binding!!.gameWonExitButton.setOnClickListener { view : View ->
                model.onReplay()
                model.setRoomName("")
                model.resetUser()
                view.findNavController().navigate(R.id.loginFragment)
            }
            model.highscore.observe(viewLifecycleOwner, {
                createCardPlayer(it)
            })
        }

    }
    private fun createCardPlayer(userList: List<User>) {
        val inflater = LayoutInflater.from(this.context)

        userList.forEach {
            val userCard = inflater.inflate(R.layout.user_card,null) as CardView

            userCard.findViewById<TextView>(R.id.user_name).text = it.name
            userCard.findViewById<TextView>(R.id.user_status).text = it.score.toString()

            userCard.cardElevation = 10F
            val params = ViewGroup.MarginLayoutParams(ViewGroup.MarginLayoutParams.MATCH_PARENT, ViewGroup.MarginLayoutParams.WRAP_CONTENT)
            params.setMargins(20, 10, 20, 30)
            userCard.layoutParams = params
            binding?.listPlayerHighscore?.addView(userCard)
        }
    }

}