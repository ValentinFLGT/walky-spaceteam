package com.example.walkyspaceteam.screens.game

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Switch
import android.widget.TableRow
import androidx.annotation.RequiresApi
import androidx.fragment.app.activityViewModels
import com.squareup.seismic.ShakeDetector
import androidx.navigation.findNavController
import com.example.walkyspaceteam.R
import com.example.walkyspaceteam.databinding.FragmentGameBinding
import com.example.walkyspaceteam.model.elements.UIElement
import com.example.walkyspaceteam.model.elements.UIType
import com.example.walkyspaceteam.model.events.Event
import com.example.walkyspaceteam.network.services.sendMessage
import com.example.walkyspaceteam.viewmodel.GameViewModel

class GameFragment : Fragment(), ShakeDetector.Listener {

    private var binding: FragmentGameBinding? = null

    private val model: GameViewModel by activityViewModels()

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val fragmentBinding = FragmentGameBinding.inflate(inflater, container, false)
        binding = fragmentBinding

        setHasOptionsMenu(true)

        val sensorManager =
            this.activity?.applicationContext?.getSystemService(SensorManager::class.java) as SensorManager

        val shakeDectector = ShakeDetector(this)

        shakeDectector.start(sensorManager)

        return binding!!.root

    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            lifecycleOwner = viewLifecycleOwner

            model.getGameStarted().observe(viewLifecycleOwner, { it ->
                createDashboardUI(it)
                binding?.gameTitle?.text =
                    "Vous voyagez actuellement dans la galaxie n°" + model.currentLevel.value.toString()
            })

            model.getNextAction().observe(viewLifecycleOwner, { it ->
                model.onEventShakeComplete()
                resetAndStartTime(it.time)
                binding!!.action.text = it.sentence
                if(model.username.value.toString() == "Henri1"){
                    val el = it.uiElement
                    model.webSocket.value?.let {
                        sendMessage(it, Event.PlayerAction(el))
                    }
                }
            })

            model.getGameOver().observe(viewLifecycleOwner, { it ->
                gameFinished(it.score, it.win)
                model.webSocket.value?.close(1000, null)
            })

            model.getNextLevel().observe(viewLifecycleOwner, { it ->
                goToNextLevel(it)
            })
        }
    }

    private fun resetAndStartTime(time: Long) {
        binding?.progressBar?.progress = 0
        ObjectAnimator.ofInt(binding?.progressBar, "progress", 100)
            .setDuration(time)
            .start()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun createDashboardUI(moduleList: List<UIElement>) {
        binding?.tableLayout?.removeAllViews()

        val uiElementMap = mutableMapOf<Int, List<UIElement>>()

        moduleList.chunked(2).forEachIndexed { index, list ->
            uiElementMap[index] = list
        }

        uiElementMap.map {
            val row = TableRow(this.context)
            row.gravity = 17

            it.value.forEach { element ->
                if (element.uiType != UIType.SHAKE) {
                    val element = createUiElement(element)
                    println(element)
                    row.addView(element)
                }
            }

            binding?.tableLayout?.addView(row)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun createUiElement(el: UIElement): View {

        var element = View(this.context)
        val params = TableRow.LayoutParams(500, 145).also { it.setMargins(25, 25, 25, 25) }

        when (el.uiType) {
            UIType.BUTTON -> {
                element = Button(this.context)
                element.text = el.content
                element.setOnClickListener {
                    model.webSocket.value?.let {
                        sendMessage(it, Event.PlayerAction(el))
                    }
                }
                element.layoutParams = params
            }

            UIType.SWITCH -> {
                element = Switch(this.context)
                element.text = el.content
                element.textAlignment = View.TEXT_ALIGNMENT_CENTER
                element.setBackgroundColor(resources.getColor(R.color.gray, null))
                element.setOnClickListener {
                    model.webSocket.value?.let {
                        sendMessage(it, Event.PlayerAction(el))
                    }
                }
                element.layoutParams = params
            }
        }

        return element
    }

    private fun gameFinished(score: Int, win: Boolean) {

        model.setScore(score)
        model.webSocket.value?.close(1000, null)

        if (win) {
            view?.findNavController()?.navigate(R.id.gameWonFragment)
        } else {
            view?.findNavController()?.navigate(R.id.gameOverFragment)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    fun goToNextLevel(element: Event.NextLevel) {

        createDashboardUI(element.uiElementList)
        model.setCurrentLevel(element.level)

        binding?.gameTitle?.text =
            "Vous voyagez actuellement dans la galaxie n°" + model.currentLevel.value.toString()
    }

    override fun hearShake() {

        if (model.eventShake.value == false) {
            model.webSocket.value?.let {
                sendMessage(it, Event.PlayerAction(UIElement(1, UIType.SHAKE, ("Bouge"))))
            }

            model.onEventShake()
        }

    }

}


