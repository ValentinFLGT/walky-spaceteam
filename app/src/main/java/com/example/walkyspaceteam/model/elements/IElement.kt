package com.example.walkyspaceteam.model.elements

interface IElement {
    var id: Int
    val content: String
}