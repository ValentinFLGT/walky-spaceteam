package com.example.walkyspaceteam.model.events

import com.squareup.moshi.Json

enum class EventType() {

    @Json(name = "GAME_STARTED")
    GAME_STARTED(),

    @Json(name = "GAME_OVER")
    GAME_OVER(),

    @Json(name = "ERROR")
    ERROR(),

    @Json(name = "READY")
    READY(),

    @Json(name = "NEXT_ACTION")
    NEXT_ACTION(),

    @Json(name = "NEXT_LEVEL")
    NEXT_LEVEL(),

    @Json(name = "WAITING_FOR_PLAYER")
    WAITING_FOR_PLAYER(),

    @Json(name = "PLAYER_ACTION")
    PLAYER_ACTION()

}