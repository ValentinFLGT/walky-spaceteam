package com.example.walkyspaceteam.model.users

data class User(
    val id: Int,
    val name: String,
    val avatar: String,
    var score: Int,
    var state: State = State.OVER
)