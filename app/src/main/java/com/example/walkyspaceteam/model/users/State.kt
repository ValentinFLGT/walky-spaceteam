package com.example.walkyspaceteam.model.users

enum class State(var value: Int) {
    WAITING(0), READY(1), IN_GAME(2), OVER(3)
}