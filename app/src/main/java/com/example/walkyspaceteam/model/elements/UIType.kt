package com.example.walkyspaceteam.model.elements

enum class UIType {
    BUTTON, SWITCH, SHAKE
}