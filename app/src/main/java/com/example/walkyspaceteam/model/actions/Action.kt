package com.example.walkyspaceteam.model.actions

import com.example.walkyspaceteam.model.elements.UIElement

data class Action(
    val sentence: String,
    val uiElement: UIElement,
    val time: Long = 8000
)