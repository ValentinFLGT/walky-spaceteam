# Walky spaceteam 🪐👽

Here is our realization of the DIM Spaceteam project based on the real game SpaceTeam. 

### How it works ?

A server push event to the app using websocket. 
Each event represent a state of the game. A game of SpaceDim is composed by multiple event :

- WAITING_FOR_PLAYER, users are in a waiting room until there are unless 2 ready users.
- GAME_STARTED, beginning of the game with UI elements list to generate (buttons, switch, shake).
- NEXT_ACTION represent the action that the user must accomplished until the end of countdown clicking the correct UI element.
- NEXT_LEVEL a new UI elements to generate
- GAME_OVER end of the game (win or loose) with score and reached level

- LoginFragment : the home page where player sign in or sign up
- GameFragment : responsible for the gameplay and the generation of elements according to the received events.
- GameWonFragment : Win screen displayed if players score at least 800 points
- GameOverFragment : Lose screen displayed if players score is negative
- WaitingRoomFragment the waiting room with user list and button to be ready
- Websocket connexion is handled in WebSocketClient. All fragment uses the GameViewModel which hold the LiveData (event)

### Requirements

- Android Studio

### Installation

- Clone the repo
- Build and run the project

### Tools

OkHttp for websocket and HTTP requests
Moshi as content negociator
Seismic for ShakeListener
Timber for easy logging